using System;
using System.Net;
using Calculator.Api.Domain.Responses;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xunit;

namespace ContractTests
{
    public class Calculator
    {
        [Fact]
        [Trait("Category", "Contract")]
        public void Calculator_calculate_additionOperation_correctResultReturned()
        {
            RestClient restClient = new RestClient("http://localhost:5000");
            RestRequest request = new RestRequest("/calculator", Method.POST);

            request.AddJsonBody(new
            {
                FirstOperand = 7,
                SecondOperand = 5,
                Operator = "+"
            });

            IRestResponse response = restClient.Execute(request);
            var jsonResponse = JObject.Parse(response.Content).ToObject<CalculationResponse>();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(12, jsonResponse.Result);
        }

        [Fact]
        [Trait("Category", "Contract")]
        public void Calculator_calculate_subtractionOperation_correctResultReturned()
        {
            RestClient restClient = new RestClient("http://localhost:5000");
            RestRequest request = new RestRequest("/calculator", Method.POST);

            request.AddJsonBody(new
            {
                FirstOperand = 7,
                SecondOperand = 5,
                Operator = "-"
            });

            IRestResponse response = restClient.Execute(request);
            var jsonResponse = JObject.Parse(response.Content).ToObject<CalculationResponse>();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(2, jsonResponse.Result);
        }

        [Fact]
        [Trait("Category", "Contract")]
        public void Calculator_calculate_multiplicationOperation_correctResultReturned()
        {
            RestClient restClient = new RestClient("http://localhost:5000");
            RestRequest request = new RestRequest("/calculator", Method.POST);

            request.AddJsonBody(new
            {
                FirstOperand = 7,
                SecondOperand = 5,
                Operator = "x"
            });

            IRestResponse response = restClient.Execute(request);
            var jsonResponse = JObject.Parse(response.Content).ToObject<CalculationResponse>();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(35, jsonResponse.Result);
        }

        [Fact]
        [Trait("Category", "Contract")]
        public void Calculator_calculate_divisionOperation_correctResultReturned()
        {
            RestClient restClient = new RestClient("http://localhost:5000");
            RestRequest request = new RestRequest("/calculator", Method.POST);

            request.AddJsonBody(new
            {
                FirstOperand = 15,
                SecondOperand = 5,
                Operator = "÷"
            });

            IRestResponse response = restClient.Execute(request);
            var jsonResponse = JObject.Parse(response.Content).ToObject<CalculationResponse>();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(3, jsonResponse.Result);
        }
    }
}
