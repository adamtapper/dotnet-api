﻿using System;
namespace Calculator.Api.Domain.Models
{
    public class CalculationModel
    {
        public double FirstOperand { get; set; }
        public double? SecondOperand { get; set; }
        public string Operator { get; set; }
    }
}
