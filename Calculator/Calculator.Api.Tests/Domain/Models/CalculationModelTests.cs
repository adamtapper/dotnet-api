﻿using System;
using System.Reflection;
using Calculator.Api.Domain.Models;
using Xunit;

namespace Calculator.Api.Tests.Domain.Models
{
    public class CalculationModelTests
    {
        public bool IsNullable<T>(T t) { return false; }
        public bool IsNullable<T>(T? t) where T : struct { return true; }

        /*
            UNIT TESTS
        */

        [Fact]
        [Trait("Category", "Unit")]
        public void constructor_calculationModel_threeFieldsExist()
        {
            CalculationModel calculationModel = new CalculationModel();

            PropertyInfo[] properties = calculationModel.GetType().GetProperties();

            Assert.Equal("FirstOperand", properties[0].Name);
            Assert.Equal("SecondOperand", properties[1].Name);
            Assert.Equal("Operator", properties[2].Name);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void constructor_calculationModel_secondOperandIsNullable()
        {
            CalculationModel calculationModel = new CalculationModel();

            Assert.True(IsNullable(calculationModel.SecondOperand));
        }
    }
}
