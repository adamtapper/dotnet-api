﻿using System;
using Xunit;
using Calculator.Api.Domain.Responses;
using System.Reflection;

namespace Calculator.Api.Tests.Domain.Responses
{
    public class CalculationResponseTests
    {
        /*
             UNIT TESTS
         */

        [Fact]
        [Trait("Category", "Unit")]
        public void constructor_calculationResponse_oneFieldExists()
        {
            CalculationResponse calculationResponse = new CalculationResponse();

            PropertyInfo[] properties = calculationResponse.GetType().GetProperties();

            Assert.Equal("Result", properties[0].Name);
        }
    }
}
