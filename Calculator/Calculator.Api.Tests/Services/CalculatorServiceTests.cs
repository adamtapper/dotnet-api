﻿using System;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;
using Calculator.Api.Services;
using Xunit;

namespace Calculator.Api.Tests.Services
{
    public class CalculatorServiceTests
    {
        private readonly CalculatorService calculatorService = new CalculatorService();

        [Theory]
        [InlineData(0, 12, 12)]
        [InlineData(3, 0, 3)]
        [InlineData(09, 3, 12)]
        [InlineData(9, 06, 15)]
        [InlineData(8, 2, 10)]
        [InlineData(49, 7, 56)]
        [InlineData(64, 16, 80)]
        [InlineData(58.4411764, 633.783783, 692.2249594)]
        [InlineData(2345, 3.7, 2348.7)]
        [InlineData(9.32, 3.1789, 12.4989)]
        [InlineData(-6, -98, -104)]
        [InlineData(-34, 36, 2)]
        [InlineData(76, -509, -433)]
        [InlineData(902, -87, 815)]
        [InlineData(-78.54, -65.46, -144)]
        [InlineData(-98.12, 76.39, -21.730000000000004)]
        [Trait("Category", "Unit")]
        public void calculate_additionOperation_resultReturnedAsDouble(double firstOperand, double secondOperand, double result)
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = firstOperand;
            model.SecondOperand = secondOperand;
            model.Operator = "+";

            CalculationResponse expectedResponse = new CalculationResponse();
            expectedResponse.Result = result;

            CalculationResponse actualResponse = calculatorService.calculate(model);

            Assert.Equal(expectedResponse.Result, actualResponse.Result);
        }

        [Theory]
        [InlineData(0, 65, -65)]
        [InlineData(5, 0, 5)]
        [InlineData(09, 3, 6)]
        [InlineData(9, 06, 3)]
        [InlineData(4, 8, -4)]
        [InlineData(7, 2, 5)]
        [InlineData(67, 82, -15)]
        [InlineData(512, 347, 165)]
        [InlineData(9.6, 3.7, 5.8999999999999995)]
        [InlineData(3.765, 1.765, 2)]
        [InlineData(98.723, 27.7, 71.023)]
        [InlineData(27.7, 98.723, -71.023)]
        [InlineData(807.914, 98.54, 709.374)]
        [InlineData(-6, -98, 92)]
        [InlineData(-34, 36, -70)]
        [InlineData(76, -509, 585)]
        [InlineData(902, -87, 989)]
        [InlineData(-78.54, -65.46, -13.080000000000013)]
        [InlineData(-98.12, 76.39, -174.51)]
        [Trait("Category", "Unit")]
        public void calculate_subtractionOperation_resultReturnedAsDouble(double firstOperand, double secondOperand, double result)
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = firstOperand;
            model.SecondOperand = secondOperand;
            model.Operator = "-";

            CalculationResponse expectedResponse = new CalculationResponse();
            expectedResponse.Result = result;

            CalculationResponse actualResponse = calculatorService.calculate(model);

            Assert.Equal(expectedResponse.Result, actualResponse.Result);
        }

        [Theory]
        [InlineData(0, 8, 0)]
        [InlineData(45, 0, 0)]
        [InlineData(09, 3, 27)]
        [InlineData(9, 06, 54)]
        [InlineData(7, 8, 56)]
        [InlineData(2, 1, 2)]
        [InlineData(9, 4, 36)]
        [InlineData(67, 82, 5494)]
        [InlineData(512, 347, 177664)]
        [InlineData(9.6, 3.7, 35.52)]
        [InlineData(-6, -98, 588)]
        [InlineData(-34, 36, -1224)]
        [InlineData(76, -509, -38684)]
        [InlineData(902, -87, -78474)]
        [InlineData(-78.54, -65.46, 5141.2284)]
        [InlineData(-98.12, 76.39, -7495.3868)]
        [Trait("Category", "Unit")]
        public void calculate_multiplicationOperation_resultReturnedAsDouble(double firstOperand, double secondOperand, double result)
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = firstOperand;
            model.SecondOperand = secondOperand;
            model.Operator = "x";

            CalculationResponse expectedResponse = new CalculationResponse();
            expectedResponse.Result = result;

            CalculationResponse actualResponse = calculatorService.calculate(model);

            Assert.Equal(expectedResponse.Result, actualResponse.Result);
        }

        [Theory]
        [InlineData(0, 1, 0)]
        [InlineData(6, 0, 0)]
        [InlineData(09, 3, 3)]
        [InlineData(9, 06, 1.5)]
        [InlineData(5, 4, 1.25)]
        [InlineData(49, 7, 7)]
        [InlineData(64, 16, 4)]
        [InlineData(1987, 34, 58.44117647058823)]
        [InlineData(2345, 3.7, 633.7837837837837)]
        [InlineData(9.3, 3.1, 3)]
        [InlineData(-6, -98, 0.061224489795918366)]
        [InlineData(-34, 36, -0.9444444444444444)]
        [InlineData(76, -509, -0.14931237721021612)]
        [InlineData(-45, -15, 3)]
        [InlineData(42, -21, -2)]
        [InlineData(-78.54, -65.46, 1.1998166819431717)]
        [InlineData(-98.12, 76.39, -1.2844613169262993)]
        [Trait("Category", "Unit")]
        public void calculate_divisionOperation_resultReturnedAsDouble(double firstOperand, double secondOperand, double result)
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = firstOperand;
            model.SecondOperand = secondOperand;
            model.Operator = "÷";

            CalculationResponse expectedResponse = new CalculationResponse();
            expectedResponse.Result = result;

            CalculationResponse actualResponse = calculatorService.calculate(model);

            Assert.Equal(expectedResponse.Result, actualResponse.Result);
        }
    }
}
