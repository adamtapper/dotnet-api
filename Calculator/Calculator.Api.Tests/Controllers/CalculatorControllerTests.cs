﻿using System;
using Moq;
using Xunit;
using Calculator.Api.Controllers;
using Calculator.Api.Services;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace BusinessLogicTests.Controllers
{
    public class CalculatorControllerTests
    {
        private Mock<ICalculatorService> mockCalculatorService = new Mock<ICalculatorService>();
        private CalculatorController calculatorController;


        /*
            UNIT TESTS
        */

        [Fact]
        [Trait("Category", "Unit")]
        public void post_calculationModel_calculatorServiceCalled()
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = 6;
            model.SecondOperand = 9;
            model.Operator = "+";

            CalculationResponse fakeResponse = new CalculationResponse();
            fakeResponse.Result = 12;

            mockCalculatorService.Setup(service => service.calculate(It.IsAny<CalculationModel>())).Returns(fakeResponse);
            calculatorController = new CalculatorController(mockCalculatorService.Object);

            ObjectResult response = calculatorController.Post(model).Result as ObjectResult;

            Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
            Assert.Equal(fakeResponse, response.Value);
        }

        /*
            COMPONENT TESTS
        */

        [Theory]
        [InlineData("+", 15)]
        [InlineData("-", 3)]
        [InlineData("x", 54)]
        [InlineData("÷", 1.5)]
        [Trait("Category", "Component")]
        public void post_validOperator_doubleResultReturned(string symbol, double result)
        {
            CalculationModel model = new CalculationModel();
            model.FirstOperand = 9;
            model.SecondOperand = 6;
            model.Operator = symbol;

            CalculationResponse expectedResponse = new CalculationResponse();
            expectedResponse.Result = result;

            calculatorController = new CalculatorController(new CalculatorService());

            ObjectResult response = calculatorController.Post(model).Result as ObjectResult;
            CalculationResponse actualResponse = response.Value as CalculationResponse;

            Assert.Equal(StatusCodes.Status200OK, response.StatusCode);
            Assert.Equal(expectedResponse.Result, actualResponse.Result);
        }
    }
}
